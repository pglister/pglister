#!/bin/bash
set -e
ROOT=$(dirname $(dirname $(readlink -f $0)))
SERVICES="bounceprocessor cleanup eximcleaner mailprocessor mailsender moderationprocessor"

if [ "$1" == "-install" ]; then
    for S in $SERVICES ; do
	sed -e "s@_ROOT_@$ROOT@" pglister_$S.template > /etc/systemd/system/pglister_$S.service
    done
    systemctl daemon-reload
elif [ "$1" == "-enable" ]; then
    for S in $SERVICES ; do
	systemctl enable pglister_$S
    done
elif [ "$1" == "-start" ]; then
    for S in $SERVICES; do
	systemctl start pglister_$S
    done
elif [ "$1" == "-stop" ]; then
    for S in $SERVICES; do
	systemctl stop pglister_$S
    done
elif [ "$1" == "-remove" ]; then
    for S in $SERVICES; do
	systemctl stop pglister_$S
	systemctl disable pglister_$S
	rm /etc/systemd/system/pglister_$S.service
    done
    systemctl daemon-reload
else
    echo "Usage: install.sh <-install|-enable|-start|-stop|-remove>"
    exit 1
fi
