#!/usr/bin/python3 -u
#
# inject.py - inject messages into the system
#
# This script is intended to be run from exim, delivering
# new messages into the system.
#
# It does absolutely minimal parsing to get the message in.
#
# It will read the complete message into memory, so it expects
# the MTA in front of it to limit sizes to "something reasonable".
#

import os
import sys
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config
from baselib.misc import log


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Inject message")
    parser.add_argument('-d', '--destination', dest='dest', help='Envelope recipient', required=True)
    parser.add_argument('-s', '--sender', dest='sender', help='Envelope sender', required=True)
    parser.add_argument('-m', '--msgid', dest='msgid', help='Message-id', required=True)

    args = parser.parse_args()

    try:
        conn = config.conn('inject')
        curs = conn.cursor()
    except Exception as e:
        # Exim will only log the first row, so get something there
        print("Connection exception: %s" % e.message)
        # Show the full exception when run manually or in exim debug mode
        raise e

    if args.msgid == '':
        print("Message-id cannot be empty")
        sys.exit(1)

    try:
        # If there's a + in the address, it's a bounce (it's the only ones
        # we get that have a + - for VERP), so split it out into a separate
        # table already here (as the other one is going to be more transient).
        if args.dest.find('+') >= 0:
            curs.execute("INSERT INTO bounce_mail (recipient, sender, messageid, dt, contents) VALUES (%(recipient)s, %(sender)s, %(messageid)s, now(), %(contents)s)", {
                'recipient': args.dest.lower(),
                'sender': args.sender.lower(),
                'messageid': args.msgid,
                'contents': sys.stdin.buffer.read(),
            })
            curs.execute("NOTIFY bounce")
            log(curs, 0, 'inject', 'New bounce from {0} to {1}.'.format(args.sender, args.dest), args.msgid)
        else:
            curs.execute("INSERT INTO incoming_mail (recipient, sender, messageid, dt, contents) VALUES (%(recipient)s, %(sender)s, %(msgid)s, now(), %(contents)s)", {
                'recipient': args.dest.lower(),
                'sender': args.sender.lower(),
                'msgid': args.msgid,
                'contents': sys.stdin.buffer.read(),
            })
            curs.execute("NOTIFY incoming")
            log(curs, 0, 'inject', 'New mail from {0} to {1}.'.format(args.sender, args.dest), args.msgid)
        conn.commit()
        conn.close()
    except Exception as e:
        # Exim will only log the first row, so get something there
        print("Connection exception: %s" % e.message)
        # Show the full exception when run manually or in exim debug mode
        raise e
