from mailutil.charset import clean_charset
from mailutil.html import flatten_html


def get_truncated_body(msg):
    """
    Get a truncated plaintext representation of the body.

    This is used for things like moderation messages and bounce messages, so it does
    not have to be perfect. But we want to make sure we don't end up sending along
    an encoded mess that can't be read.
    """

    # Why 10000? Well, it has to be something...
    return _get_plaintext_body(msg)[:10000]


def _get_plaintext_body(msg):
    # Most of this code stolen from the pgarchives code, with slight modifications
    # around the fact that we don't really care for all of the body or any of the
    # attachments at this point.
    b = _get_payload_as_unicode(msg)
    if b:
        return b

    b = _recursive_first_plaintext(msg)
    if b:
        return b

    # No plaintext found, so see what we can do about HTML. We ensure we flatten the
    # html into the closest thing to plaintext we can find.
    b = _recursive_first_plaintext(msg, True)
    if b:
        return flatten_html(b)

    return "Could not find message body"


def _get_payload_as_unicode(msg):
    b = msg.get_payload(decode=True)
    if b:
        # Look for a charset
        charset = None
        params = msg.get_params()
        if not params:
            # No content-type, assume ascii
            return str(b, 'us-ascii', errors='ignore')
        for k, v in params:
            if k.lower() == 'charset':
                charset = v
                break
        if charset:
            try:
                return str(b, clean_charset(charset), errors='ignore')
            except Exception:
                return "Failed to get unicode payload"

        # Actual charset not specified, so assume ascii
        return str(b, 'us-ascii', errors='ignore')

    # If it was nothing, well then it didn't work...
    return b


def _recursive_first_plaintext(container, html_instead=False):
    pl = container.get_payload()
    if isinstance(pl, str):
        # Not a multipart message, not supposed to get here...
        return None

    for p in pl:
        if p.get_params() is None:
            # MIME multipart/mixed, but no MIME type on the part
            return _get_payload_as_unicode(p)
        if p.get_params()[0][0].lower() == 'text/plain':
            # Skip if it's an attachment
            if p.get('Content-Disposition', '').startswith('attachment'):
                continue
            return _get_payload_as_unicode(p)
        if p.is_multipart():
            b = _recursive_first_plaintext(p, html_instead)
            if b:
                return b
        if html_instead and p.get_params()[0][0].lower() == 'text/html':
            return _get_payload_as_unicode(p)

    # Found nothing, so give up
    return None
