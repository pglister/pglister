from .mailhandler import MailHandler
from baselib.misc import log


class ModerationQueueHandler(object):
    def __init__(self, conn):
        self.conn = conn

    def process_next(self):
        with self.conn.cursor() as curs:
            curs.execute("SELECT id, listid, sender, recipient, messageid, contents, subject, blocknotifications FROM moderation WHERE approved ORDER BY id LIMIT 1 FOR UPDATE")
            ll = curs.fetchall()
            if len(ll) == 0:
                # Nothing left, so we're done
                self.conn.rollback()
                return False
            elif len(ll) != 1:
                raise Exception("Should not happen - LIMIT 1 returned more than 1")

            # Else we have an actual moderation to process
            try:
                id, listid, sender, recipient, messageid, contents, subject, blocknotifications = ll[0]
                mail = MailHandler(self.conn, -1, recipient, sender, messageid, contents)
                if mail.process_from_moderation(listid, subject, blocknotifications):
                    curs.execute("DELETE FROM moderation WHERE id=%(id)s", {'id': id})
                    log(curs, 0, 'mod', 'Mail released from moderation.', messageid)
                else:
                    # Failure! That's not good. Move the message back to the queue,
                    # and log an error.
                    curs.execute("UPDATE moderation SET approved='f' WHERE id=%(id)s", {'id': id})
                    log(curs, 2, 'mod', 'Failed to release mail from moderation!', messageid)
            except Exception as ex:
                import traceback
                print("Exception: %s" % ex)
                print(traceback.format_exc())
                self.conn.rollback()
                curs.execute("UPDATE moderation SET approved='f' WHERE id=%(id)s", {'id': id})
                self.conn.commit()
            else:
                # New transaction for the next mail
                self.conn.commit()
        return True
