from collections import defaultdict
import re
import subprocess

from baselib.misc import log

re_sender = re.compile(r'^<([^@]+)-owner\+M(\d+)-\d+@(.*)>')
re_modnotice = re.compile(r'^(.*)-notice\+M(\d+)-([a-z0-9]+)@(.*)$', re.I)


class EximCleanerHandler(object):
    def __init__(self, conn):
        self.conn = conn

    def process(self):
        with self.conn.cursor() as curs:
            # Get the queue of everything to potentially remove
            toremove = {}
            idhandled = []
            removecounter = defaultdict(int)
            removenoticecounter = 0
            curs.execute("""SELECT u.id, l.name AS listname, d.name AS domain, u.subscriber_id, a.email
FROM lists_unsubscribelog u
INNER JOIN lists_list l ON l.id=u.list_id
INNER JOIN lists_domain d ON d.id=l.domain_id
INNER JOIN lists_subscriberaddress a ON a.id=u.subscriber_id""")
            for id, listname, domain, subid, email in curs.fetchall():
                toremove[(listname, domain, subid)] = email
                idhandled.append(id)

            # Get the mail queue
            p = subprocess.run(['/usr/bin/mailq', ], check=True, capture_output=True, text=True)
            for l in p.stdout.splitlines():
                pieces = l.split()
                if len(pieces) == 4:
                    # 4 entries means it's the info we need
                    eximid = pieces[2]
                    m = re_sender.match(pieces[3])
                    if m:
                        # OK, this looks like one of our regular outgoing VERPed emails
                        matchtup = (m.group(1), m.group(3), int(m.group(2)))
                        if matchtup in toremove:
                            # Remove the email. If it fails to remove we ignore the problem -- it
                            # could be as simple as the email has now been delivered.
                            subprocess.run(['/usr/sbin/exim', '-Mrm', eximid], check=False)
                            removecounter[toremove[matchtup]] += 1
                    m = re_modnotice.match(pieces[3])
                    if m:
                        # Matched a moderation notice bounce. If this moderation item has already
                        # been dropped (discarded or approved), and the "your email is moderated" note
                        # is still in the queue, we drop it. For discarded this is "always right", and
                        # it's the common case. We can't separate that from "approved" at this level,
                        # so in case for example the "your mail is moderated" notice has been graylisted,
                        # we may drop it before it's delivered.
                        modid = int(m.group(2))
                        curs.execute("SELECT id FROM moderation WHERE id=%(modid)s", {
                            'modid': modid,
                        })
                        if curs.rowcount == 0:
                            # This entry is no longer in the moderation queue, so drop it
                            subprocess.run(['/usr/sbin/exim', '-Mrm', eximid], check=False)
                            removenoticecounter += 1

            # Log a summary per email that was cleaned, if any
            for k, v in removecounter.items():
                log(
                    curs,
                    0,
                    'eximclean',
                    "Removed {} emails for {} from exim queue because of unsubscription".format(v, k),
                )
            if removenoticecounter:
                log(
                    curs,
                    0,
                    'eximclean',
                    "Removed {} moderation notices from exim queue because of moderation entry no longer present".format(removenoticecounter),
                )

            # Clean out the queue
            if idhandled:
                curs.execute("DELETE FROM lists_unsubscribelog WHERE id=ANY(%(ids)s)", {
                    'ids': idhandled,
                })

            self.conn.commit()

            print("Completed one run")
