# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LogEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('t', models.DateTimeField(verbose_name=b'Timestamp', db_index=True)),
                ('level', models.IntegerField(default=0)),
                ('source', models.CharField(max_length=10)),
                ('username', models.CharField(max_length=30, null=True, blank=True)),
                ('messageid', models.CharField(max_length=1000, null=True, blank=True)),
                ('msg', models.CharField(max_length=1000)),
            ],
            options={
                'ordering': ('-t',),
                'db_table': 'log',
                'managed': False,
            },
        ),
    ]
