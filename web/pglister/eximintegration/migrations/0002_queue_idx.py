# Generated by Django 2.2.11 on 2020-11-18 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eximintegration', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            "CREATE UNIQUE INDEX queue_eximid_key ON eximintegration.queue(eximid)",
            "DROP INDEX eximintegration.queue_eximid_key",
        ),
    ]
