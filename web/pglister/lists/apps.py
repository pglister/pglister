from django.apps import AppConfig


class ListsAppConfig(AppConfig):
    name = 'pglister.lists'

    def ready(self):
        from .migration import handle_user_data
        from pglister.auth import auth_user_data_received

        auth_user_data_received.connect(handle_user_data)
