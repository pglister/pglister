from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from lib.baselib.lists import ModerationReason, ModerationOption

from pglister.lists.models import CannedRejectResponse

import html
import io

register = template.Library()


@register.filter(is_safe=True)
def label_class(value, arg):
    return value.label_tag(attrs={'class': arg})


@register.filter(is_safe=True)
def field_class(value, arg):
    return value.as_widget(attrs={"class": arg})


@register.filter(name='alertmap')
@stringfilter
def alertmap(value):
    if value == 'error':
        return 'alert-danger'
    elif value == 'warning':
        return 'alert-warning'
    elif value == 'success':
        return 'alert-success'
    else:
        return 'alert-info'


@register.filter(is_safe=True)
def ischeckbox(obj):
    return obj.field.widget.__class__.__name__ == "CheckboxInput" and not getattr(obj.field, 'regular_field', False)


@register.filter(is_safe=True)
def isrequired_error(obj):
    if obj.errors and obj.errors[0] == "This field is required.":
        return True
    return False


@register.filter(is_safe=True)
def splitlines(obj):
    return obj.split("\n")


@register.filter
def moderation_reason(obj, extra):
    return ModerationReason(obj, extra).full_string()


@register.simple_tag(takes_context=True)
def moderation_options(context, reason, is_subscribed):
    s = io.StringIO()

    # This is ugly but it works...
    for i, t in ModerationReason(int(reason)).get_moderation_options(is_subscribed, context['user'].is_superuser):
        if i == ModerationOption.REJECT:
            s.write('<optgroup label="Reject">')
            s.write('<option value="{}">{}</option>'.format(ModerationOption.REJECT, 'Reject'))
            if '__cannedresponses' not in context:
                context['__cannedresponses'] = CannedRejectResponse.objects.all()
            for r in context['__cannedresponses']:
                s.write('<option value="-{}" title="{}">{}</option>'.format(r.id, html.escape(r.content), r.title))
            s.write('</optgroup>')
        else:
            s.write('<option value="{}">{}</option>'.format(i, t))

    return mark_safe(s.getvalue())


@register.filter
def replacepercent(obj, replacewith):
    return obj.replace('%', replacewith)
