# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0023_messageid_moderation'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ['list', 'tag'],
            },
        ),
        migrations.AddField(
            model_name='list',
            name='tagged_delivery',
            field=models.BooleanField(default=False, verbose_name='Use tags for delivery'),
        ),
        migrations.AddField(
            model_name='list',
            name='tagkey',
            field=models.CharField(max_length=100, verbose_name='Hash key for tags', blank=True),
        ),
        migrations.AddField(
            model_name='list',
            name='taglistsource',
            field=models.URLField(max_length=100, verbose_name='URL to list of tags', blank=True),
        ),
        migrations.AddField(
            model_name='listtag',
            name='list',
            field=models.ForeignKey(to='lists.List', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='listsubscription',
            name='tags',
            field=models.ManyToManyField(to='lists.ListTag', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='listtag',
            unique_together=set([('list', 'tag')]),
        ),
    ]
