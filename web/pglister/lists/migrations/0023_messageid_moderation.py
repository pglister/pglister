# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0022_apikey_notnull'),
    ]

    operations = [
        migrations.CreateModel(
            name='MessageidModerationList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('messageid', models.CharField(unique=True, max_length=1000, validators=[pglister.lists.models.ValidateCleanMessageId])),
                ('addedat', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
