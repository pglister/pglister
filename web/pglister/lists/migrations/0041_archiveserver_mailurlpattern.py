# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-04-30 10:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0040_blocknotifications'),
    ]

    operations = [
        migrations.AddField(
            model_name='archiveserver',
            name='mailurlpattern',
            field=models.CharField(null=True, blank=True, max_length=200),
        ),
        migrations.RunSQL("UPDATE lists_archiveserver SET mailurlpattern=urlpattern"),
        migrations.AlterField(
            model_name='archiveserver',
            name='mailurlpattern',
            field=models.CharField(max_length=200),
        ),
    ]
