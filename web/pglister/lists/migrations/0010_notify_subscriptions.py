# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.sizefield


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0009_maxsize'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='notify_subscriptions',
            field=models.BooleanField(default=False, help_text="Send notification email to moderators on subscriptions and unsubscriptions"),
        ),
        migrations.AlterField(
            model_name='list',
            name='maxsizedrop',
            field=pglister.lists.sizefield.SizeField(help_text='Larger than this will be silently dropped. Enter with KB, MB, GB as necessary. NULL means default.', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='list',
            name='maxsizemoderate',
            field=pglister.lists.sizefield.SizeField(help_text='Larger than this will be moderated. Enter with KB, MB, GB as necessary. NULL means default.', null=True, blank=True),
        ),
    ]
