# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0024_tagged_delivery'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='send_moderation_notices',
            field=models.BooleanField(default=True, help_text='Send notices to authors of emails held for moderation)'),
        ),
    ]
