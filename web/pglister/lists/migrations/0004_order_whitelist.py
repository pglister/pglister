# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0003_list_moderation_regex'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='listwhitelist',
            options={'ordering': ('list', 'address')},
        ),
    ]
