from django import forms


class _OptionDescriptionMixin:
    def add_option_descriptions(self, r):
        # Build a dict of the descriptions. We have to loop over the inside queryset
        # (somewhat breaking the abstraction), because looping over the choices themselves
        # will already cut it to a tuple of (id, name).
        descs = {t.id: t.description for t in self.choices.queryset}

        # XXX: doesn't really support option groups, just straight options!
        for og in r['widget']['optgroups']:
            if og[1][0]['value']:  # Skip past the None option
                # Django 3.1 changed this to return a ModelChoiceIteratorValue, so unwrap this one
                # if necessary.
                v = og[1][0]['value']
                if hasattr(v, 'value'):
                    v = v.value
                og[1][0]['description'] = descs[int(v)]

        return r


class DescribedCheckboxSelectMultiple(forms.CheckboxSelectMultiple, _OptionDescriptionMixin):
    option_template_name = 'widgets/described_checkbox_select_multiple_option.html'

    def get_context(self, name, value, attrs):
        r = super().get_context(name, value, attrs)

        return self.add_option_descriptions(r)


class SelectWithOptionTitle(forms.Select, _OptionDescriptionMixin):
    option_template_name = 'widgets/select_with_option_title_option.html'

    def get_context(self, name, value, attrs):
        r = super().get_context(name, value, attrs)

        return self.add_option_descriptions(r)
