from django.db import connection

import psycopg2.extras


def _get_cursor():
    curs = connection.cursor()

    # Django debug mode will wrap the cursor. And then the django debug toolbar will wrap it
    # again. So unwrap until we have an actual psycopg2 cursor.
    psycocurs = curs
    while hasattr(psycocurs, 'cursor'):
        psycocurs = psycocurs.cursor

    # Django 3.1.1 (YES! In a security update!!) intentionally breaks PostgreSQL jsonb support
    # for access that's not through the ORM. Un-break this by turning the support back on,
    # but do so specifically for the cursor that we're using in this query only, so that django
    # can keep the broken behavior for its cursors.
    psycopg2.extras.register_default_jsonb(conn_or_curs=psycocurs)
    return curs


def exec_to_list(query, params=None):
    curs = _get_cursor()
    curs.execute(query, params)
    return curs.fetchall()


def exec_to_row(query, params=None):
    curs = _get_cursor()
    curs.execute(query, params)
    return curs.fetchone()


def exec_to_dict(query, params=None):
    curs = _get_cursor()
    curs.execute(query, params)
    columns = [col[0] for col in curs.description]
    return [dict(zip(columns, row))for row in curs.fetchall()]


def exec_to_scalar(query, params=None):
    curs = _get_cursor()
    curs.execute(query, params)
    r = curs.fetchone()
    if r:
        return r[0]
    # If the query returns no rows at all, then just return None
    return None


def exec_to_scalar_dict(query, params=None):
    return exec_to_dict(query, params)[0]


def exec_to_hier(query, params=None):
    # XXX: make configurable to do this at more than one level?
    # XXX: assumes that query comes sorted by first columns, and that
    #      the first column is unique.
    curs = _get_cursor()
    curs.execute(query, params)
    columns = [col[0] for col in curs.description[1:]]
    data = {}
    for row in curs:
        data[row[0]] = dict(zip(columns, row[1:]))
    return data
