from django.urls import include
from django.urls import path, re_path
from django.contrib import admin
from django.conf import settings

from pglister.lists.views import Home, Manage, Unqueue, UnqueueToken, Subscribe, Unsubscribe
from pglister.lists.views import EditSub, UnsubscribeConfirm, UnsubscribeLink, Testmail
from pglister.lists.views import ViewMembership, UnsubscribeHelp
from pglister.lists.views_api import ListApi, ArchivesApi, SubscribersApi
from pglister.lists.views_addr import AddMail, ConfirmMail, DelMail, Resend, Block
from pglister.lists.views_moderate import Moderate, ModerateMail, ModerateList, ModerateScript
from pglister.lists.views_moderate import BlocklistDomain
from pglister.lists.views_manage import ManageList, ManageSubscribers, ManageSubscribersUnsubscribe
from pglister.lists.views_manage import ManageBounces, ManageGlobalSubscribers, ManageThreadBlocklist
from pglister.lists.views_manage import ManageModerators

import pglister.auth

urlpatterns = [
    re_path(r'^$', Home.as_view()),
    re_path(r'^manage/$', Manage.as_view()),
    re_path(r'^manage/unqueue/(?P<modid>\d+)/$', Unqueue.as_view()),
    re_path(r'^manage/unqueue/(?P<usertoken>[a-z0-9]+)/$', UnqueueToken.as_view()),
    re_path(r'^manage/subscribe/$', Subscribe.as_view()),
    re_path(r'^manage/edit/(?P<pk>\d+)/$', EditSub.as_view()),
    re_path(r'^manage/members/(?P<listid>\d+)/$', ViewMembership.as_view()),
    re_path(r'^manage/unsubscribe/(?P<subid>\d+)-(?P<listid>\d+)/$', Unsubscribe.as_view()),
    re_path(r'^manage/testmail/(?P<subid>\d+)-(?P<listid>\d+)/$', Testmail.as_view()),
    re_path(r'^manage/addmail/$', AddMail.as_view()),
    re_path(r'^manage/delmail/(?P<subid>\d+)/$', DelMail.as_view()),
    re_path(r'^manage/resend/(?P<subid>\d+)/$', Resend.as_view()),
    re_path(r'^manage/block/(?P<subid>\d+)/$', Block.as_view()),
    re_path(r'^manage/confirmmail/(?P<token>[a-z0-9]+)/$', ConfirmMail.as_view()),

    # Moderation
    re_path(r'^moderate/$', Moderate.as_view()),
    re_path(r'^moderate/(?P<j>[a-z0-9-]+)\.js$', ModerateScript.as_view()),
    re_path(r'^moderate/(?P<token>[a-z0-9]+)/(?P<result>approve|whitelist|discard|preview)/$', ModerateMail.as_view()),
    re_path(r'^listmoderate/(?P<token>[a-z0-9]+)/(?P<result>approve|discard)/$', ModerateList.as_view()),
    re_path(r'^moderate/manage/(?P<id>[0-9]+)/$', ManageList.as_view()),
    re_path(r'^moderate/manage/(?P<id>[0-9]+)/view/$', ManageSubscribers.as_view()),
    re_path(r'^moderate/manage/(?P<id>[0-9]+)/view/unsub/$', ManageSubscribersUnsubscribe.as_view()),
    re_path(r'^moderate/manage/global/search/$', ManageGlobalSubscribers.as_view()),
    re_path(r'^moderate/manage/blockthreads/$', ManageThreadBlocklist.as_view()),
    re_path(r'^moderate/manage/moderators/$', ManageModerators.as_view()),
    re_path(r'^moderate/bounces/(?P<listid>\d+)-(?P<sub>\d+)/$', ManageBounces.as_view()),
    re_path(r'^moderate/blocklist_domains/$', BlocklistDomain.as_view()),

    # Unsubscribe (which does not authenticate)
    re_path(r'^unsubscribe/$', UnsubscribeHelp.as_view()),
    re_path(r'^unsub/(?P<listid>\d+)/(?P<token>[a-z0-9]+)/$', UnsubscribeLink.as_view()),
    re_path(r'^unsubscribe-confirm/(?P<token>[a-z0-9]+)/$', UnsubscribeConfirm.as_view()),

    # REST API calls
    re_path(r'^api/list/(?P<listname>[a-z0-9@\.-]+)/$', ListApi.as_view()),
    re_path(r'^api/subscribers/(?P<listname>[a-z0-9@\.-]+)/$', SubscribersApi.as_view()),
    re_path(r'^api/archive/(?P<servername>[a-zA-Z0-9]+)/(?P<op>[a-z]+)/$', ArchivesApi.as_view()),

    # Auth system integration
    re_path(r'^(?:accounts/)?login/?$', pglister.auth.login),
    re_path(r'^(?:accounts/)?logout/?$', pglister.auth.logout),
    re_path(r'^auth_receive/$', pglister.auth.auth_receive),
    re_path(r'^auth_api/$', pglister.auth.auth_api),


    re_path(r'^admin/', admin.site.urls),
]


if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
